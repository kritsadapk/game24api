export const game24 = (nums: number[]): boolean => {
  if (nums.length === 1) {
    return +nums[0] === 24;
  }

  let result: boolean = false;

  for (let i in nums) {
    let num = nums[i];
    let usedNums = [...nums];
    usedNums.splice(+i, 1);
    for (let j in usedNums) {
      let multiply = [...usedNums];
      let divide = [...usedNums];
      let subtract = [...usedNums];
      let add = [...usedNums];

      multiply[+j] *= num;
      divide[+j] /= num;
      subtract[+j] -= num;
      add[+j] += num;

      result =
        result ||
        game24(multiply) ||
        game24(add) ||
        game24(subtract) ||
        game24(divide);
    }
  }
  return result;
};
