import experss, { Request, Response } from "express";
import { game24 } from "./game24";
const app = experss();
const port = 8080;

app.post("/", (req: Request, res: Response) => {
  const num = req.body?.num;
  if (!num) {
    return res.status(400).json({
      message: "require array of number",
    });
  }
  const arrNum: [] = num;
  if (!arrNum.length && arrNum.length < 4) {
    return res.status(400).json({
      message: "require array length of 4",
    });
  }
  return game24(arrNum) ? "YES" : "NO";
});
// console.log(game24([5, 5, 5, 5]));
app.listen(port, () => {
  console.log(`server start at port ${port}`);
});
